#ifndef _DSL_H
#define _DSL_H

#include <inttypes.h>

extern uint8_t dsl_RSSI;		// Received signal strength indicator
extern uint8_t dsl_Battery; 	// Battery voltage (0-255 [0V - 8.2V])
extern uint8_t dsl_Allocation; // Frequency allocation (35,40,72)

#define USART1_BAUD 38400
// this function should be called within the UART RX ISR
extern void dsl_parser(uint8_t c);

#endif //_DSL_H

