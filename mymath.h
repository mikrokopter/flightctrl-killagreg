#ifndef _MYMATH_H
#define _MYMATH_H

#include <inttypes.h>

extern int16_t c_sin_8192(int16_t angle);
extern int16_t c_cos_8192(int16_t angle);
extern int16_t c_atan2(int16_t y, int16_t x);

#endif // _MYMATH_H
