#ifndef _TIMER2_H
#define _TIMER2_H

#include <inttypes.h>

extern volatile int16_t ServoNickValue;
extern volatile int16_t ServoRollValue;

void TIMER2_Init(void);
void Servo_On(void);
void Servo_Off(void);

#endif //_TIMER2_H

