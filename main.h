#ifndef _MAIN_H
#define _MAIN_H

#include <avr/io.h>

#define ATMEGA644	0
#define ATMEGA644P	1

#define SYSCLK F_CPU



// neue Hardware
#define RED_OFF   {if((BoardRelease == 10)||(BoardRelease == 20)) PORTB &=~(1<<PORTB0); else  PORTB |= (1<<PORTB0);}
#define RED_ON    {if((BoardRelease == 10)||(BoardRelease == 20)) PORTB |= (1<<PORTB0); else  PORTB &=~(1<<PORTB0);}
#define RED_FLASH PORTB ^= (1<<PORTB0)
#define GRN_OFF   {if(BoardRelease  < 12) PORTB &=~(1<<PORTB1); else PORTB |= (1<<PORTB1);}
#define GRN_ON    {if(BoardRelease  < 12) PORTB |= (1<<PORTB1); else PORTB &=~(1<<PORTB1);}
#define GRN_FLASH PORTB ^= (1<<PORTB1)

#include <inttypes.h>

extern uint8_t LowVoltageWarning;
extern uint8_t BoardRelease;
extern uint8_t CPUType;
extern uint16_t FlightMinutes, FlightMinutesTotal;
void LipoDetection(uint8_t print);

#endif //_MAIN_H






