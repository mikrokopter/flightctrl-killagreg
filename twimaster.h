#ifndef _I2C_MASTER_H
#define _I2C_MASTER_H

#include <inttypes.h>

#define TWI_STATE_MOTOR_TX 			0
#define TWI_STATE_MOTOR_RX 			3
#define TWI_STATE_GYRO_OFFSET_TX	7

extern volatile uint8_t twi_state;
extern volatile uint8_t motor_write;
extern volatile uint8_t motor_read;

extern uint8_t MissingMotor;

#define MAX_MOTORS	12

typedef struct
{
	uint8_t SetPoint; // written by attitude controller
	uint8_t Present;  // 1 if BL was found
	uint8_t Error;    // I2C error counter
	uint8_t Current;  // read back from BL
	uint8_t MaxPWM;   // read back from BL
} __attribute__((packed)) MotorData_t;

extern MotorData_t Motor[MAX_MOTORS];

extern volatile uint16_t I2CTimeout;

extern void I2C_Init (void); // Initialize I2C
extern void I2C_Start(uint8_t start_state); // Start I2C
extern void I2C_Stop (uint8_t start_state); // Stop I2C
extern void I2C_Reset(void); // Reset I2C

#endif
