#ifndef _ANALOG_H
#define _ANALOG_H

#include <inttypes.h>

extern volatile uint16_t MeasurementCounter;
extern volatile int16_t UBat;
extern volatile int16_t AdValueGyroNick, AdValueGyroRoll, AdValueGyroYaw;
#define HIRES_GYRO_AMPLIFY 8 // the offset corrected HiResGyro values are a factor of 8 scaled to the AdValues
extern volatile int16_t HiResGyroNick, HiResGyroRoll;
extern volatile int16_t FilterHiResGyroNick, FilterHiResGyroRoll;
extern volatile int16_t AdValueAccRoll, AdValueAccNick, AdValueAccTop, AdValueAccZ;
#define AIR_PRESSURE_SCALE 18 // 1 ADC counts corresponds approx. to 18 cm
#define SM_FILTER 16
extern volatile int32_t SumHeight;			// filter for variometer
extern volatile int32_t AirPressure;		// gets less with growing height
extern volatile int32_t StartAirPressure;	// air pressure at ground
extern volatile int32_t ReadingHeight;		// height according to air pressure (altimeter in steps of 1cm)
extern volatile int16_t ReadingVario;
extern volatile int16_t AdAirPressure; 	// ADC value of air pressure measurement
#define EXPANDBARO_OPA_OFFSET_STEP 10
#define EXPANDBARO_ADC_SHIFT (-523)			// adc shift per EXPANDBARO_OPA_OFFSET_STEP
extern uint8_t PressureSensorOffset;
extern int8_t ExpandBaro;

extern volatile uint8_t ADReady;

extern uint8_t DacOffsetGyroNick, DacOffsetGyroRoll, DacOffsetGyroYaw;

#define AIR_PRESSURE_SEARCH 800
#define AIR_PRESSURE_SEARCH_MIN 750
#define AIR_PRESSURE_SEARCH_MAX 950

void SearchAirPressureOffset(void);
void SearchDacGyroOffset(void);
void ADC_Init(void);


// clear ADC enable & ADC Start Conversion & ADC Interrupt Enable bit
#define ADC_Disable() (ADCSRA &= ~((1<<ADEN)|(1<<ADSC)|(1<<ADIE)))
// set ADC enable & ADC Start Conversion & ADC Interrupt Enable bit
#define ADC_Enable() (ADCSRA |= (1<<ADEN)|(1<<ADSC)|(1<<ADIE))


#endif //_ANALOG_H


